const supertest = require('supertest');
const app =  require('../app');

/*describe('Probar las rutas de usuarios', () => {
    it('Deberia de obtener la lista de usuarios', function(done){
      supertest(app).get('/users/')
      .set('Authorization', 'bearer eyJhbGciOiJIUzI1NiJ9.NWY3ZGU2YmZlODM0OTgwODA1ODkyMGJm.o77bojBPOurdoD5oPANUt3jf8F2cZiAN96VJpQ296eg')
      .end(async function(err, res){
        expect(res.statusCode).equalsTo(200);
        done();
      });
    });
});*/


describe('Probar las rutas de usuarios', () => {
    it('Deberia de obtener la lista de usuarios', (done) => {
      supertest(app).get('/users/')
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiJ9.NWY3ZGU2YmZlODM0OTgwODA1ODkyMGJm.o77bojBPOurdoD5oPANUt3jf8F2cZiAN96VJpQ296eg')
      .end(function(err, response){
	        if (err) {
	            done(err);
	        } else {
	            expect(response.statusCode).toEqual(200);
	            done();
	        }
	    });
    });
});
